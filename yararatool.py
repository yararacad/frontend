#!/usr/bin/env python
import sys
import yararacad as yc
import importlib.util
from typing import Callable
from inspect import getfullargspec, FullArgSpec
from functools import reduce

class Flag:
	def __init__(self, name, char=None):
		self.name = name
		self.char = char
		self.value = False

	def set(self):
		self.value = True

class Parameter(Flag):
	def __init__(self, name, char=None, type=None, value=None):
		super().__init__(name, char)
		self.value = value
		self.type = type

	def convert_type(self, value):
		return (self.type if self.type is not None else lambda v : v)(value)

	def set(self, value):
		self.value = self.convert_type(value)

def handle_short(shortopts, opt: str, args: iter):
	try:
		shortopts[opt[0]].set()
		if len(opt) > 1:
			handle_short(shortopts, opt[1:], args)
	except TypeError: #option requires an argument
		if len(opt) < 1:
			shortopts[opt[0]].set(opt[1:])
		else:
			shortopts[opt].set(next(args))

def handle_long(longopts, opt: str, args: iter):
	try:
		longopts[opt].set()
	except (TypeError, KeyError): #option requires an argument
		try:
			opt, value = opt.split('=')
		except ValueError: #option uses space instead of '='
			value = next(args)

		longopts[opt].set(value)

def main():
	opts = [ Parameter('export', 'x', str) ]
	longopts = { o.name: o for o in opts }
	shortopts = { o.char: o for o in opts if o.char }

	args = iter(sys.argv[1:])
	for arg in args:
		if arg[0] == '-':
			if arg[1] == '-':
				handle_long(longopts, arg[2:], args)
			else:
				handle_short(shortopts, arg[1:], args)
		else:
			model = import_file(arg)
			parameters = {}
			for arg in args:
				parameters[arg.strip('-')] = next(args)
			name, output = get_model_output(model)
			if isinstance(output, Callable):
				output = output(**parameters)
			yc.export_stl(output, name)

def valid_model_outputs(model_state: iter):
	for (k, v) in model_state:
		if returns_valid_output(v):
			yield(k, v)
			yield from [(k, f) for (k, f) in model_state if returns_valid_output(f)]
		if is_valid_output(v):
			yield (k, v)

def get_model_output(model):
	model_state = iter(model.__dict__.items())
	return list(valid_model_outputs(model_state))[-1]

def get_parameters(f: Callable):
	spec = getfullargspec(f)
	for i in range(len(spec.args)):
		name = spec.args[i]
		char = name if len(name) == 1 else None
		try:
			value = spec.defaults[i]
		except IndexError:
			value = None
		try:
			type = spec.annotations[name]
		except KeyError:
			type = None

		yield Parameter(name, char, type, value)

def basename_without_extension(path):
	return '.'.join(path.split('/')[-1].split('.')[:-1])

def import_file(path):
	name = basename_without_extension(path)
	spec = importlib.util.spec_from_file_location(name, path)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	return module

def return_type(f: Callable):
	return f.__annotations__["return"]

def is_valid_output(o):
	try:
		return issubclass(o, yc.Solid)
	except TypeError:
		return False

def returns_valid_output(f: Callable):
	return isinstance(f, Callable) and is_valid_output(return_type(f))

if __name__ == "__main__":
	main()
