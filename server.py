import yararacad
from yararatool import get_parameters, get_model_output, Parameter
from bottle import route, request, run, template, static_file, get
import os

import open_top_box

def form_input(name, type='text', value=""):
	type = html_type(type)
	value_field = template('value="{{value}}"', value=value) if value else ""
	return template('''
		<div class ="form">
		<label>{{name}}:</label>
		<input name="{{name}}" type="{{type}}" {{!value_field}}>
		</div>
		''', name=name, type=type, value_field=value_field)

def html_type(type):
	try:
		return{str : 'text', int : 'number'}[type]
	except KeyError:
		return type

def form(*params: Parameter):
	return template('''
		<link rel="stylesheet" type="text/css" href="/static/style.css" />
		<div class="split right">
		<form action="/" method="post">
			{{!inputs}}
		 	<input value="SUBMIT" type="submit" />
		</form>
		</div>
	''', inputs=''.join([form_input(name=p.name, type = p.type, value=p.value) for p in params]))

def render_img():
	return '''
		<div class="split left">
			<img src="static/output.png">
		</divs>
		'''

def update_values(*params, **kwargs):
	for p in params:
		if p.name in kwargs:
			p.value = kwargs[p.name]
		yield p

def export_model(**kwargs):
	output = 'static/' + name
	yararacad.export_stl(
		o(**kwargs),
		output
	)
	return output + '.stl'

def render_image(out_path, in_path):
	#os.system('f3d --camera-azimuth-angle=45 --camera-elevation-angle=45 --up=+Z --output=%s %s' % (out_path, in_path))
	os.system('f3d --camera-position=10,10,10 --up=+Z --no-background -e --output=%s %s' % (out_path, in_path))

name, o = get_model_output(open_top_box)
@route('/')
def get_submit(**initial_values):
	some_junk = update_values(*get_parameters(o), **initial_values)
	print(some_junk)
	return form(*some_junk), render_img()

@route('/', method='POST')
def post_submit():
	render_image('static/output.png', export_model(**request.forms))
	return get_submit(**request.forms)

@route('/static/<filename>')
def send_static(filename):
	return static_file(filename, root="static")

run(host='localhost', port=8080)
